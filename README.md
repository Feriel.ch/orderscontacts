Symfony Demo Application
The "Symfony Demo Application" is a reference application created to show how to develop applications following the Symfony Best Practices.

You can also learn about these practices in the official Symfony Book.

Requirements
PHP 7.2.0 or higher;
and the usual Symfony application requirements.

Download Symfony to install the symfony binary on your computer and run this command:

$ symfony new --demo my_project

Alternatively, you can use Composer:

$ composer create-project symfony/symfony-demo my_project

The steps to follow
#1-Install the dependencies with Composer:

composer install

#1-Database configuration:

The database connection settings are stored in the DATABASE_URL variable that exists in the .env file. 
Example:
DATABASE_URL=‘mysql://db_user:db_password@127.0.0.1:3306/db_name’
db_user: root
db_password: par défaut vide
db_name: nom de votre base par exemple 'shop'

#2- Database creation :

php bin/console doctrine:database:create

#3- Migrations: Creation of database tables / schemas

a- php bin/console make:migration
b- php bin/console doctrine:migrations:migrate

#4- Project execution

symfony server

Then access the application in your browser at the given URL (https://localhost:8000 by default).

#5- Routes

orders-to-csv-api 
orders-to-csv
login
register
