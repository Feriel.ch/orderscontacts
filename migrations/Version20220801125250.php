<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220801125250 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE items_orders (id VARCHAR(255) NOT NULL, amount DOUBLE PRECISION NOT NULL, description LONGTEXT NOT NULL, item VARCHAR(255) NOT NULL, item_description LONGTEXT NOT NULL, quantity INT NOT NULL, unit_code VARCHAR(255) NOT NULL, unit_description VARCHAR(255) NOT NULL, unit_price DOUBLE PRECISION NOT NULL, vatamount DOUBLE PRECISION NOT NULL, vatpercentage DOUBLE PRECISION NOT NULL, discount DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE items_orders');
    }
}
