<?php

namespace App\Entity;

use App\Repository\ItemsOrdersRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ItemsOrdersRepository::class)
 */
class ItemsOrders
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $Amount;

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->Amount;
    }

    /**
     * @param mixed $Amount
     */
    public function setAmount($Amount): void
    {
        $this->Amount = $Amount;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @ORM\Column(type="text")
     */
    private $Description;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Item;

    /**
     * @ORM\Column(type="text")
     */
    private $ItemDescription;

    /**
     * @ORM\Column(type="integer")
     */
    private $Quantity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $UnitCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $UnitDescription;

    /**
     * @ORM\Column(type="float")
     */
    private $UnitPrice;

    /**
     * @ORM\Column(type="float")
     */
    private $VATAmount;

    /**
     * @ORM\Column(type="float")
     */
    private $VATPercentage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $Discount;

    public function getId(): ?string
    {
        return $this->id;
    }


    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }


    public function getItem(): ?string
    {
        return $this->Item;
    }

    public function setItem(string $Item): self
    {
        $this->Item = $Item;

        return $this;
    }

    public function getItemDescription(): ?string
    {
        return $this->ItemDescription;
    }

    public function setItemDescription(string $ItemDescription): self
    {
        $this->ItemDescription = $ItemDescription;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->Quantity;
    }

    public function setQuantity(int $Quantity): self
    {
        $this->Quantity = $Quantity;

        return $this;
    }

    public function getUnitCode(): ?string
    {
        return $this->UnitCode;
    }

    public function setUnitCode(string $UnitCode): self
    {
        $this->UnitCode = $UnitCode;

        return $this;
    }

    public function getUnitDescription(): ?string
    {
        return $this->UnitDescription;
    }

    public function setUnitDescription(string $UnitDescription): self
    {
        $this->UnitDescription = $UnitDescription;

        return $this;
    }

    public function getUnitPrice(): ?float
    {
        return $this->UnitPrice;
    }

    public function setUnitPrice(float $UnitPrice): self
    {
        $this->UnitPrice = $UnitPrice;

        return $this;
    }

    public function getVATAmount(): ?float
    {
        return $this->VATAmount;
    }

    public function setVATAmount(float $VATAmount): self
    {
        $this->VATAmount = $VATAmount;

        return $this;
    }

    public function getVATPercentage(): ?float
    {
        return $this->VATPercentage;
    }

    public function setVATPercentage(float $VATPercentage): self
    {
        $this->VATPercentage = $VATPercentage;

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->Discount;
    }

    public function setDiscount(?float $Discount): self
    {
        $this->Discount = $Discount;

        return $this;
    }
}
