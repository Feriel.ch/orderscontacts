<?php

namespace App\Controller;

use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\RegistrationType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="security_registration")
     */
    public function register(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        //registration with email and password
        $uuid = Uuid::uuid4(); //generate uuid
        $uuidGenerator = $uuid->toString();
        $user = new User();
        $user->setId($uuidGenerator);
        $form = $this->createForm(RegistrationType::class, $user); // RegistrationType is the registration form
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $hash = $encoder->encodePassword($user, $user->getPassword()); //Hash password
            $user->setPassword($hash);
            $entityManager->persist($user); // tell Doctrine you want to (eventually) save the User (no queries yet)
            $entityManager->flush(); //actually executes the queries (i.e. the INSERT query)
            return $this->redirectToRoute('security_login'); //after register redirect to login path

        }
        return $this->render('security/register.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/login", name="security_login")
     */
    public function login(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        //login with username and password
        return $this->render('security/login.html.twig');
    }


}
