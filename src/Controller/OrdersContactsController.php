<?php

namespace App\Controller;

use App\Entity\ItemsOrders;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\OrdersProcessed;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\env;

class OrdersContactsController extends AbstractController
{
    private $client;

    public function __construct(HttpClientInterface $client, EntityManagerInterface $entityManager)
    {
        $this->client = $client;

    }
    /**
     * @Route("/orders", name="app_orders")
     */
    public function fetchOrdersInformation()
    {
        //dd($this->getParameter('app.x_api_key'));
        $response = $this->client->request(
            'GET',
            $this->getParameter('app.orders_api'),
            ['headers' => [
                'x-api-key' => $this->getParameter('app.x_api_key')],]
        );
        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];
        $content = $response->getContent();
        $newResponse = new Response(
            $content,
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
        return $newResponse;
    }
    /**
     * @Route("/contacts", name="app_contacts_orders")
     */
    public function fetchContactsInformation()
    {
        $response = $this->client->request(
            'GET',
            $this->getParameter('app.contacts_api'),
            ['headers' => [
                'x-api-key' => $this->getParameter('app.x_api_key'),
            ],]
        );
        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];
        $content = $response->getContent();
        $newResponse = new Response(
            $content,
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
        return $newResponse;

    }
    /**
     * @Route("/orders-to-csv-api", name="app_orders_contacts")
     */
    public function ContactsOrders(EntityManagerInterface $entityManager)
    {

        $contacts = $this->fetchContactsInformation(); // data from fetchContactsInformation function
        $orders = $this->fetchOrdersInformation(); // data from fetchOrdersInformation function
        $data = json_decode($orders->getContent(), true)['results'];
        $data2 = json_decode($contacts->getContent(), true)['results'];
        $array =[];
        foreach ($data as $key=>$datum) { //get Orders data
            foreach ($datum['SalesOrderLines'] as $value){ //get sales order lines data of Orders
                foreach ($value as $value1){ //get data from value table
                    $uuid = Uuid::uuid4();
                    $uuidGenerator = $uuid->toString();
                    $itemsOrders = new ItemsOrders(); //Insert Data to ItemsOrders table
                    $itemsOrders->setId($uuidGenerator);
                    $itemsOrders->setAmount($value1['Amount']);
                    $itemsOrders->setDescription($value1['Description']);
                    $itemsOrders->setDiscount($value1['Discount']);
                    $itemsOrders->setItem($value1['Item']);
                    $itemsOrders->setItemDescription($value1['ItemDescription']);
                    $itemsOrders->setQuantity($value1['Quantity']);
                    $itemsOrders->setUnitCode($value1['UnitCode']);
                    $itemsOrders->setUnitDescription($value1['UnitDescription']);
                    $itemsOrders->setUnitPrice($value1['UnitPrice']);
                    $itemsOrders->setVATAmount($value1['VATAmount']);
                    $itemsOrders->setVATPercentage($value1['VATPercentage']);
                    $entityManager->persist($itemsOrders);
                    $entityManager->flush();
                }
            }
            foreach ($data2 as $dat){ ////get contacts data
                if($datum['DeliverTo'] == $dat['ID']){ // Retrieve Data where DeliverTo = ID of contacts
                    $array[$key]['order']=$datum['OrderID'];
                    $array[$key]['delivery_name']=$dat['AccountName'];
                    $array[$key]['delivery_address']=$dat['AddressLine1'];
                    $array[$key]['delivery_country']=$dat['Country'];
                    $array[$key]['delivery_zipcode']=$dat['ZipCode'];
                    $array[$key]['delivery_city']=$dat['City'];
                    $array[$key]['item_index']=$datum['SalesOrderLines']['results'][$key]['Item'];
                    $array[$key]['item_id']=$datum['SalesOrderLines']['results'][$key]['Item'];
                    $array[$key]['item_quantity']=$datum['SalesOrderLines']['results'][$key]['Quantity'];
                    $array[$key]['line_price_excl_vat']=$datum['SalesOrderLines']['results'][$key]['UnitPrice'];
                    $array[$key]['line_price_incl_vat']=$datum['SalesOrderLines']['results'][$key]['UnitPrice']+$datum['SalesOrderLines']['results'][$key]['VATAmount'];
                    $orderProcessed = new OrdersProcessed(); //insert data to OrdersProcessed table
                    $orderProcessed->setId($uuidGenerator);
                    $orderProcessed->setOrders($datum['OrderID']);
                    $orderProcessed->setAccountName($dat['AccountName']);
                    $orderProcessed->setAddressLine1($dat['AddressLine1']);
                    $orderProcessed->setCountry($dat['Country']);
                    $orderProcessed->setCity($dat['City']);
                    $orderProcessed->setDeliveryName($dat['ContactName']);
                    $orderProcessed->setItem($datum['SalesOrderLines']['results'][$key]['Item']);
                    $orderProcessed->setItemIndex(1);
                    $orderProcessed->setZipCode($dat['ZipCode']);
                    $orderProcessed->setQuantity($datum['SalesOrderLines']['results'][$key]['Quantity']);
                    $orderProcessed->setUnitPrice($datum['SalesOrderLines']['results'][$key]['UnitPrice']);
                    $orderProcessed->setVatAmount($datum['SalesOrderLines']['results'][$key]['UnitPrice']+$datum['SalesOrderLines']['results'][$key]['VATAmount']);
                    $entityManager->persist($orderProcessed);
                    $entityManager->flush();
                }
            }
        }

        $fp = fopen("sample.csv", "w"); //Open file sample.csv in write mode
        foreach ($array as $line)
        {
            // dump($line);
            fputcsv(
                $fp, // The file pointer
                $line // The fields
            );
        }
        fclose($fp); // close file
        return $this->json([
            'status' => 200,
            'message' => '',
        ], 400
        );
    }

    /**
     * @Route("/orders-to-csv", name="app_orderss")
     */
    public function orderstocsv()
    {
        return $this->render('orders.html.twig'); //Button to execute the ContactsOrders function
    }

}
